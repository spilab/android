package es.unex.politecnica.spilab.csprofile;

/**
 * Created by brijesh on 20/4/17.
 */

public class Constants {

    public static String MQTT_BROKER_URL = "tcp://192.168.0.29:1883";

    public static final String PUBLISH_TOPIC = "profile";

    public static String CLIENT_ID = "none";

    public static final String DEFAULT_PROFILE = "{\n" +
            "   \"hasId\":\"1\",\n" +
            "   \"hasName\":\"Username\",\n" +
            "   \"IPAddress\":\"0.0.0.0\",\n" +
            "   \"macAddress\":\"00:00:00:00:00\",\n" +
            "   \"Operation\":[\n" +
            "      {\n" +
            "         \"hasName\":\"g_light_mode\",\n" +
            "         \"powerValue\":\"on\"\n" +
            "      },\n" +
            "      {\n" +
            "         \"hasName\":\"g_light_luminosity\",\n" +
            "         \"powerValue\":\"100\"\n" +
            "      },\n" +
            "      {\n" +
            "         \"hasName\":\"g_light_rgb\",\n" +
            "         \"powerValue\":\"FFFFFF\"\n" +
            "      },\n" +
            "      {\n" +
            "         \"hasName\":\"g_temp\",\n" +
            "         \"powerValue\":\"22,5\"\n" +
            "      }\n" +
            "   ],\n" +
            "   \"Service\":[\n" +
            "      {\n" +
            "         \"hasName\":\"sk_cleanac\",\n" +
            "         \"hasAddress\":\"...\",\n" +
            "         \"realStateValue\":\"off\"\n" +
            "      }\n" +
            "   ]\n" +
            "}";
}


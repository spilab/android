package es.unex.politecnica.spilab.csprofile.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import es.unex.politecnica.spilab.csprofile.fragments.ConfigFragment;
import es.unex.politecnica.spilab.csprofile.fragments.GoalsFragment;
import es.unex.politecnica.spilab.csprofile.fragments.InfosFragment;
import es.unex.politecnica.spilab.csprofile.fragments.SkillsFragment;

/**
 * Created by tutlane on 19-12-2017.
 */

public class TabsAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public TabsAdapter(FragmentManager fm, int NoofTabs) {
        super(fm);
        this.mNumOfTabs = NoofTabs;
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
//            case 0:
//                HomeFragment home = new HomeFragment();
//                return home;
//            case 1:
//                AboutFragment about = new AboutFragment();
//                return about;
            case 0:
                ConfigFragment contact = new ConfigFragment();
                return contact;
            case 1:
                InfosFragment infos = new InfosFragment();
                return infos;
            case 2:
                GoalsFragment goals = new GoalsFragment();
                return goals;
            case 3:
                SkillsFragment skills = new SkillsFragment();
                return skills;
            default:
                return null;
        }
    }
}
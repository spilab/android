package es.unex.politecnica.spilab.csprofile.models;

public class Skill {
    private String name;
    private String currentValue;
    private String endpoint;

    public Skill(String name, String currentValue, String endpoint) {
        this.name = name;
        this.currentValue = currentValue;
        this.endpoint = endpoint;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(String currentValue) {
        this.currentValue = currentValue;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    @Override
    public String toString() {
        return "Skill{" +
                "name='" + name + '\'' +
                ", currentValue='" + currentValue + '\'' +
                ", endpoint='" + endpoint + '\'' +
                '}';
    }
}

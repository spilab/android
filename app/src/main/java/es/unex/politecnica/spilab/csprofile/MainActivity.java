package es.unex.politecnica.spilab.csprofile;

import android.Manifest;
import android.app.ActivityManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;

import android.support.v7.widget.Toolbar;
import android.text.format.Formatter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.nio.ByteOrder;
import java.util.Collections;
import java.util.List;

import es.unex.politecnica.spilab.csprofile.adapters.TabsAdapter;
import es.unex.politecnica.spilab.csprofile.models.Goal;
import es.unex.politecnica.spilab.csprofile.models.Profile;
import es.unex.politecnica.spilab.csprofile.services.MqttMessageService;
import es.unex.politecnica.spilab.csprofile.services.PahoMqttClient;

public class MainActivity extends AppCompatActivity {

    //    private static MqttAndroidClient client;
    private static String TAG = "MainActivity";
//    private static PahoMqttClient pahoMqttClient;

//    private EditText textMessage, subscribeTopic, unSubscribeTopic, txtServerIp;
//    private Button publishMessage, subscribe, unSubscribe, btnSendProfile, btnSetIp;


    private static Context ctx;

    private static MainActivity main;

    public static Profile fullProfile;
    public static String profile;
    private static String serverIp;

    private Intent mServiceIntent;

    //    private static boolean subscribed = false;
    private boolean storagePermissionsGranted = false;
    private static View v;

    private static final int STORAGE_PERMISSION_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        v = findViewById(android.R.id.content);

        ctx = getApplicationContext();
        main = this;

        // Get permissions
        getStoragePermission();

//        // Read profile
//        fullProfile = new Profile();
//        Log.d(TAG, " - Storage: " + storagePermissionsGranted);
//        if (storagePermissionsGranted) {
//            // Read profile (first, check if exists)
//            //File f = new File(getExternalFilesDir("data"), "profile.json");
//            File f = new File(Environment.getExternalStorageDirectory(), "Hassio");
//            checkFileExist(f);
//            readFileExternalStorage();
//
//            // Get device mac address
//            try {
//                JSONObject jsonProfile = new JSONObject(profile);
//                try {
//                    String macAddress = jsonProfile.getString("macAddress");
//                    Log.d(TAG, " - MAC-Profile:" + macAddress);
//                    profile = jsonProfile.toString();
//                } catch (Exception e) {
//                    String macAddress = getMacAddressUpdated();
//                    jsonProfile.put("macAddress", macAddress);
//                    Log.d(TAG, " - MAC-Device:" + macAddress);
//                    profile = jsonProfile.toString();
//
//                    f = new File(Environment.getExternalStorageDirectory(), "Hassio");
//                    writeFileExternalStorage();
//                    readFileExternalStorage();
//
//                    e.printStackTrace();
//
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//            Log.d(TAG, " - Profile with mac: " + profile);
//
//            /*MqttMessageService service = new MqttMessageService();
//            mServiceIntent = new Intent(this, service.getClass());
//            boolean run = isMyServiceRunning(service.getClass());
//            if (!isMyServiceRunning(service.getClass())) {
//                mServiceIntent.putExtra("profile", profile);
//                startService(mServiceIntent);
//            }
//            Log.d(TAG, " - Run init: " + run);*/
//
//        }
//
//        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
//        setSupportActionBar(toolbar);
//        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
//        tabLayout.addTab(tabLayout.newTab().setText("Config"));
//        tabLayout.addTab(tabLayout.newTab().setText("Info"));
//        tabLayout.addTab(tabLayout.newTab().setText("Goals"));
//        tabLayout.addTab(tabLayout.newTab().setText("Skills"));
//        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
//        final ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
//        TabsAdapter tabsAdapter = new TabsAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
//        viewPager.setAdapter(tabsAdapter);
//        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
//        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
//            @Override
//            public void onTabSelected(TabLayout.Tab tab) {
//                viewPager.setCurrentItem(tab.getPosition());
//            }
//
//            @Override
//            public void onTabUnselected(TabLayout.Tab tab) {
//
//            }
//
//            @Override
//            public void onTabReselected(TabLayout.Tab tab) {
//                viewPager.setCurrentItem(tab.getPosition());
//            }
//        });


        // Start service
//        serverIp = Constants.MQTT_BROKER_URL.split("//")[1].split(":")[0];
//
//        pahoMqttClient = new PahoMqttClient();
//        client = pahoMqttClient.getMqttClient(this, Constants.MQTT_BROKER_URL, Constants.CLIENT_ID);
//
//        // Stopping service if running
//        MqttMessageService service = new MqttMessageService();
//        mServiceIntent = new Intent(this, service.getClass());
//
//        mServiceIntent.putExtra("profile", profile);
//        startService(mServiceIntent);
        //getActivity().startService(mServiceIntent);


        //

        //startService();

    }

    private void loadContent() {
        // Read profile
        fullProfile = new Profile();
        Log.d(TAG, " - Storage: " + storagePermissionsGranted);
        if (storagePermissionsGranted) {
            // Read profile (first, check if exists)
            //File f = new File(getExternalFilesDir("data"), "profile.json");
            File f = new File(Environment.getExternalStorageDirectory(), "Hassio");
            checkFileExist(f);
            readFileExternalStorage();

            // Get device mac address
            try {
                JSONObject jsonProfile = new JSONObject(profile);
                try {
                    String macAddress = jsonProfile.getString("macAddress");
                    Log.d(TAG, " - MAC-Profile:" + macAddress);
                    profile = jsonProfile.toString();
                } catch (Exception e) {
                    String macAddress = getMacAddressUpdated();
                    jsonProfile.put("macAddress", macAddress);
                    Log.d(TAG, " - MAC-Device:" + macAddress);
                    profile = jsonProfile.toString();

                    f = new File(Environment.getExternalStorageDirectory(), "Hassio");
                    writeFileExternalStorage();
                    readFileExternalStorage();

                    e.printStackTrace();

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            Log.d(TAG, " - Profile with mac: " + profile);

            /*MqttMessageService service = new MqttMessageService();
            mServiceIntent = new Intent(this, service.getClass());
            boolean run = isMyServiceRunning(service.getClass());
            if (!isMyServiceRunning(service.getClass())) {
                mServiceIntent.putExtra("profile", profile);
                startService(mServiceIntent);
            }
            Log.d(TAG, " - Run init: " + run);*/

        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Config"));
        tabLayout.addTab(tabLayout.newTab().setText("Info"));
        tabLayout.addTab(tabLayout.newTab().setText("Goals"));
        tabLayout.addTab(tabLayout.newTab().setText("Skills"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        final ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
        TabsAdapter tabsAdapter = new TabsAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(tabsAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }
        });

        startService();
    }

    private void startService() {
        serverIp = Constants.MQTT_BROKER_URL.split("//")[1].split(":")[0];

        // Stopping service if running
        MqttMessageService service = new MqttMessageService();
        mServiceIntent = new Intent(this, service.getClass());

        mServiceIntent.putExtra("profile", profile);

        boolean run = isMyServiceRunning(service.getClass());
        Log.d(TAG, " - Run1: " + run);
        if (!isMyServiceRunning(service.getClass())) {
            //mServiceIntent.putExtra("profile", profile);
            startService(mServiceIntent);
        }
        Log.d(TAG, " - Run1: " + run);

    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("Service status", "Running");
                return true;
            }
        }
        Log.i("Service status", "Not running");
        return false;
    }

    private static String getIpAddress() {
        WifiManager wifiManager = (WifiManager) ctx.getSystemService(WIFI_SERVICE);
        int ipAddress = wifiManager.getConnectionInfo().getIpAddress();

        // Convert little-endian to big-endianif needed
        if (ByteOrder.nativeOrder().equals(ByteOrder.LITTLE_ENDIAN)) {
            ipAddress = Integer.reverseBytes(ipAddress);
        }

        byte[] ipByteArray = BigInteger.valueOf(ipAddress).toByteArray();

        String ipAddressString;
        try {
            ipAddressString = InetAddress.getByAddress(ipByteArray).getHostAddress();
        } catch (UnknownHostException ex) {
            Log.e("WIFIIP", "Unable to get host address.");
            ipAddressString = null;
        }

        return ipAddressString;
    }

    private static String getMacAddressUpdated() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(Integer.toHexString(b & 0xFF) + ":");
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
            //handle exception
        }
        return "";
    }

    private void checkFileExist(File f) {
        if (!f.exists()) {
            Log.d(TAG, " - No existe la carpeta. Creando carpeta Hassio");
            f.mkdir();
            writeFileExternalStorage();
        } else {
            Log.d(TAG, " - La carpeta existe");
        }
    }

    private void getStoragePermission() {
        Log.d(TAG, "getStoragePermission: getting storage permissions");
        String[] permissionsStorage = {
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_WIFI_STATE};
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_WIFI_STATE) == PackageManager.PERMISSION_GRANTED) {
            storagePermissionsGranted = true;
            loadContent();
        } else {
            ActivityCompat.requestPermissions(this,
                    permissionsStorage,
                    STORAGE_PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult: called.");
        storagePermissionsGranted = false;

        switch (requestCode) {
            case STORAGE_PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0) {
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            storagePermissionsGranted = false;
                            Log.d(TAG, "onRequestPermissionsResult: permission failed");
                            closeNow("You must grant the permissions");
                            return;
                        }
                    }
                    Log.d(TAG, "onRequestPermissionsResult: permission granted");
                    storagePermissionsGranted = true;
                    loadContent();


//                    File myExternalFile = new File(Environment.getExternalStorageDirectory(), "Hassio");
//                    if (!myExternalFile.exists()) {
//                        Log.d(TAG, " - No existe la carpeta...");
//                        myExternalFile.mkdir();
//                    }

                    //readFileExternalStorage(myExternalFile);
                    //writeFileExternalStorage(myExternalFile);


                }
            }

        }
    }

    private void closeNow(String message) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            finishAffinity();
        } else {
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            finish();
        }
    }

    private static void reload() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            main.finishAffinity();
            main.startActivity(main.getIntent());
        } else {
            main.finish();
            main.startActivity(main.getIntent());
        }

    }

    private static void setUsername() {
        // Dialog add goal
        final View v = LayoutInflater.from(main).inflate(R.layout.dialog_username, null);
        //View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_new_goal, null);

        AlertDialog.Builder builder1 = new AlertDialog.Builder(main);
        builder1.setView(v);
        builder1.setCancelable(true);
        builder1.setTitle("Name");

        final EditText nameText = v.findViewById(R.id.nameText);
        builder1.setPositiveButton(
                "Save",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        String name = nameText.getText().toString();
                        if (name.equals(""))
                            name = "Username";

                        MainActivity.fullProfile.getInfos().get(1).setValue(name);
                        MainActivity.writeFileExternalStorage();
                        reload();
                    }
                });

        builder1.setNegativeButton(
                "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    private void readFileExternalStorage() {
        try {
            File myExternalFile = new File(Environment.getExternalStorageDirectory(), "Hassio");
            FileInputStream iStream = new FileInputStream(myExternalFile + "/profile.json");

            //Get Wifi ip
            String ip = getIpAddress();
            Log.d(TAG, " - IP: " + ip);

            //InputStream iStream = getApplicationContext().getAssets().open("profile.json");
            byte[] buffer = new byte[iStream.available()];
            iStream.read(buffer);
            ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
            byteStream.write(buffer);
            byteStream.close();
            iStream.close();
            profile = byteStream.toString();
            Log.d(TAG, " - File LOADED successfully: " + profile);

            // Fill fullProfile
            fullProfile = new Profile();
            fullProfile.converterStringToProfile(profile);
            fullProfile.getInfos().get(2).setValue(ip);
            writeFileExternalStorage();

            //Toast.makeText(MainActivity.this," - Profile LOADED successfully").show();
        } catch (Exception e) {
            Log.d(TAG, " - Error LOADING profile");
            writeFileExternalStorage();
            readFileExternalStorage();
            e.printStackTrace();
        }
    }

    public static void writeFileExternalStorage() {
        try {
            File myExternalFile = new File(Environment.getExternalStorageDirectory(), "Hassio");
            FileOutputStream fos = new FileOutputStream(myExternalFile + "/profile.json");
            boolean newProfile = false;
            if (profile == null) {
                profile = Constants.DEFAULT_PROFILE;
                fullProfile = new Profile();
                fullProfile.converterStringToProfile(profile);
                String macAddress = getMacAddressUpdated();
                fullProfile.getInfos().get(3).setValue(macAddress);
                Log.d(TAG, " - Creating default profile... " + fullProfile);
                MainActivity.setUsername();
                newProfile = true;
            } else {
                if (profile.equals("")) {
                    profile = Constants.DEFAULT_PROFILE;
                    fullProfile = new Profile();
                    fullProfile.converterStringToProfile(profile);
                    String macAddress = getMacAddressUpdated();
                    fullProfile.getInfos().get(3).setValue(macAddress);
                    Log.d(TAG, " - Creating default profile... " + fullProfile);
                    setUsername();
                    newProfile = true;
                }
            }
            //fos.write(profile.getBytes());

            JSONObject jsonProfile = fullProfile.convertProfileToJson();
            Log.d(TAG, " - 2FullProfile: " + jsonProfile.toString());


            fos.write(jsonProfile.toString().getBytes());
            fos.close();
            Log.d(TAG, " - File WRITED successfully");

            Snackbar.make(v, "Profile saved successfully!", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();

        } catch (IOException e) {
            Snackbar.make(v, "Error saving the profile: " + e.getMessage(), Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            Log.d(TAG, " - Error WRITING file");
            e.printStackTrace();
        }
    }

}
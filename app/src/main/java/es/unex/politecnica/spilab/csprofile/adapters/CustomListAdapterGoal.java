package es.unex.politecnica.spilab.csprofile.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import es.unex.politecnica.spilab.csprofile.R;
import es.unex.politecnica.spilab.csprofile.models.Goal;

public class CustomListAdapterGoal extends BaseAdapter implements View.OnClickListener {
    private ArrayList<Goal> goals;
    private Context context;

    public CustomListAdapterGoal(ArrayList<Goal> goals, Context context) {
        this.goals = goals;
        this.context = context;
    }

    @Override
    public int getCount() {
        return goals.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = layoutInflater.inflate(R.layout.custom_list_view_goals_layout, null);
        ImageView option;

        Goal goal = goals.get(i);
        //option = (ImageView) view.findViewById(R.id.goalOption);
        TextView name = (TextView) view.findViewById(R.id.name);
        TextView desiredValue = (TextView) view.findViewById(R.id.desiredValue);
        name.setText(goal.getName());
        desiredValue.setText(goal.getDesiredValue());
        //option.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        /*int pos = view.getId();
        switch (view.getId()) {
            case R.id.goalOption:
                showPopupMenu(view, pos);
                break;
        }*/
    }

    // getting the popup menu
    private void showPopupMenu(View view, final int pos) {
        PopupMenu popupMenu = new PopupMenu(context, view);
        popupMenu.getMenuInflater().inflate(R.menu.option_menu, popupMenu.getMenu());
        popupMenu.show();
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.edit:

                        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo();

                        Toast.makeText(context, "Edit !" + info.position, Toast.LENGTH_SHORT).show();
                        //goals.remove(pos);
                        notifyDataSetChanged();

                        return true;
                    case R.id.remove:
                        Toast.makeText(context, "Remove !" + pos, Toast.LENGTH_SHORT).show();
                        //goals.remove(pos);
                        notifyDataSetChanged();
                        return true;
                    default:
                        return false;
                }
            }
        });
    }

    public void addGoal(Goal g) {
        goals.add(g);
        notifyDataSetChanged();
    }

    public void editGoal(Goal g, int pos) {
        goals.get(pos).setName(g.getName());
        goals.get(pos).setDesiredValue(g.getDesiredValue());
        notifyDataSetChanged();
    }

    public void deleteGoal(int pos) {
        goals.remove(pos);
        notifyDataSetChanged();
    }

    //file search result
    public void filterResult(ArrayList<Goal> newGoals) {
        goals = new ArrayList<>();
        goals.addAll(goals);
        notifyDataSetChanged();
    }

}
